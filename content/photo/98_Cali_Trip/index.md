---

title: California Trip

resources:

- src: 4.jpg
  name: San Francisco City Hall
  params:
    order: 2
    description: Capture of San Francisco City Hall while on the bus tour
    button_text: San Francisco City Hall
    button_url: "https://en.wikipedia.org/wiki/San_Francisco_City_Hall"
    timestamp: 2018-06-05 13:45


- src: 6.jpg
  name: Golden Gate
  params:
    order: 2
    description: Golden Gate capture 
    button_text: Golden Gate
    button_url: "https://en.wikipedia.org/wiki/Golden_Gate"
    timestamp: 2018-06-05 13:45


- src: 8.jpg
  name: Beautiful flower
  params:
    order: 2
    description: 
    button_text: 
    button_url: ""
    timestamp: 2018-06-05 13:45


- src: 10.jpg
  name: Beautiful Butterfly 
  params:
    order: 2
    description: Rainforests of the World - Butterfly
    button_text: California Academy of Sciences
    button_url: "https://www.calacademy.org/"
    timestamp: 2018-04-05 13:45


- src: 13.jpg
  name: Yosemite National Park
  params:
    order: 2
    description: Yosemite National Park
    button_text: Yosemite National Park
    button_url: "https://www.nps.gov/yose/index.htm"
    timestamp: 2018-06-05 13:45


- src: 14.jpg
  name: Fall at Yosemite National Park
  params:
    order: 2
    description: Fall at Yosemite National Park
    button_text: Yosemite National Park
    button_url: "https://www.nps.gov/yose/index.htm"
    timestamp: 2018-06-05 13:45


- src: 16.jpg
  name: Fall at Yosemite National Park
  params:
    order: 2
    description: Fall at Yosemite National Park
    button_text: Yosemite National Park
    button_url: "https://www.nps.gov/yose/index.htm"
    timestamp: 2018-06-05 13:45


- src: 17.jpg
  name: Half Dome at Yosemite National Park
  params:
    order: 2
    description: I wish we had enough time to do the most demanding Half Dome day hike
    button_text: Half Dome Hike
    button_url: "https://www.yosemitehikes.com/yosemite-valley/half-dome/half-dome.htm"
    timestamp: 2018-06-05 13:45

---
