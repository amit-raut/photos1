---
title: Michigan Trip

resources:

- src: 2.JPG
  name: Sleeping Bear Dunes National Lakeshore, MI
  params:
    order: 3
    description: Long day hike like you're in a dessert
    button_text: Sleeping Bear Dunes National Lakeshore, MI
    button_url: "https://www.nps.gov/slbe/index.htm"
    timestamp: 2018-05-27 14:03

- src: 3.JPG
  name: Traversity, MI
  params:
    order: 3
    description: Sunet at Traversity, MI
    button_text: Traversity, MI
    button_url: "https://www.michigan.org/city/traverse-city"
    timestamp: 2018-05-27 14:03

- src: 4.JPG
  name: Traversity, MI
  params:
    order: 3
    description: Me with friends at Traversity, MI
    button_text: Traversity, MI
    button_url: "https://www.michigan.org/city/traverse-city"
    timestamp: 2018-05-27 14:03


- src: 5.JPG
  name: Pictured Rocks
  params:
    order: 3
    description: Magical Pictured Rocks 😍
    button_text: Pictured Rocks, MI
    button_url: "https://www.nps.gov/piro/index.htm"
    timestamp: 2018-05-27 14:03

- src: 6.JPG
  name: Pictured Rocks
  params:
    order: 3
    description: Magical Pictured Rocks 😍
    button_text: Pictured Rocks, MI
    button_url: "https://www.nps.gov/piro/index.htm"
    timestamp: 2018-05-27 14:03


- src: 7.JPG
  name: Pictured Rocks
  params:
    order: 3
    description: Magical Pictured Rocks 😍
    button_text: Pictured Rocks, MI
    button_url: "https://www.nps.gov/piro/index.htm"
    timestamp: 2018-05-27 14:03


- src: 8.JPG
  name: Pictured Rocks
  params:
    order: 3
    description: Magical Pictured Rocks 😍
    button_text: Pictured Rocks, MI
    button_url: "https://www.nps.gov/piro/index.htm"
    timestamp: 2018-05-27 14:03


- src: 9.JPG
  name: Pictured Rocks
  params:
    order: 3
    description: Magical Pictured Rocks 😍
    button_text: Pictured Rocks, MI
    button_url: "https://www.nps.gov/piro/index.htm"
    timestamp: 2018-05-27 14:03


- src: 10.JPG
  name: Pictured Rocks
  params:
    order: 3
    description: Magical Pictured Rocks 😍
    button_text: Pictured Rocks, MI
    button_url: "https://www.nps.gov/piro/index.htm"
    timestamp: 2018-05-27 14:03


- src: 11.JPG
  name: Pictured Rocks
  params:
    order: 3
    description: Magical Pictured Rocks 😍
    button_text: Pictured Rocks, MI
    button_url: "https://www.nps.gov/piro/index.htm"
    timestamp: 2018-05-27 14:03


- src: 12.JPG
  name: Pictured Rocks
  params:
    order: 3
    description: Magical Pictured Rocks 😍
    button_text: Pictured Rocks, MI
    button_url: "https://www.nps.gov/piro/index.htm"
    timestamp: 2018-05-27 14:03


- src: 13.JPG
  name: Grand Island East Channel Light
  params:
    order: 3
    description: Grand Island East Channel Light opened for service in 1868
    button_text: Grand Island East Channel Light
    button_url: "https://en.wikipedia.org/wiki/Grand_Island_East_Channel_Light"
    timestamp: 2018-05-27 14:03


- src: 14.JPG
  name: Lake Michigan
  params:
    order: 3
    description: Lake Michigan
    button_text: Lake Michigan
    button_url: "https://en.wikipedia.org/wiki/Lake_Michigan"
    timestamp: 2018-05-27 14:03

- src: 15.JPG
  name: Mackinac Bridge
  params:
    order: light
    description: Susnet at Mackinac Bridge
    button_text: Mackinac Bridge
    button_url: "https://en.wikipedia.org/wiki/Mackinac_Bridge"
    timestamp: 2018-05-27 14:03

- src: 18.JPG
  name: White Shorebird
  params:
    order: 3
    description: White Shorebird at Mackinac 
    button_text: Mackinac Island
    button_url: "https://en.wikipedia.org/wiki/Mackinac_Island"
    timestamp: 2018-05-27 14:03

---
