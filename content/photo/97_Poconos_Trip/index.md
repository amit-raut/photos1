---

title: Poconos Trip

resources:

- src: 2.jpeg
  name: Delaware Water Gap
  params:
    order: 1
    description: Beautiful view of Delaware water gap while hiking on "Mount Tammany Red Dot and Blue Dot Loop Trail"
    button_text: Mount Tammany Red Dot and Blue Dot Loop Trail
    button_url: "https://www.alltrails.com/trail/us/new-jersey/mount-tammany-red-dot-and-blue-dot-loop-trail"
    timestamp: 2019-07-04 13:45

- src: 3.jpeg
  name: Bushkill Falls
  params:
    order: 1
    description: Niagara of Pennsylvania 🤪 Bushkill Falls
    button_text: Bushkill Falls
    button_url: "https://www.visitbushkillfalls.com/"
    timestamp: 2019-07-04 13:45

- src: 4.jpeg
  name: Me while hiking Mount Tammany Red Dot and Blue Dot Loop Trail
  params:
    order: 1
    description: Beautiful view of Delaware water gap while hiking on "Mount Tammany Red Dot and Blue Dot Loop Trail"
    button_text: Mount Tammany Red Dot and Blue Dot Loop Trail
    button_url: "https://www.alltrails.com/trail/us/new-jersey/mount-tammany-red-dot-and-blue-dot-loop-trail"
    timestamp: 2019-07-04 13:45

- src: 6.jpeg
  name: F.R.I.E.N.D.S
  params:
    order: 1
    description: My Poconos camping friends ❤
    button_text: Koa Campsite 
    button_url: "https://koa.com/campgrounds/delaware-water-gap/"
    timestamp: 2019-07-04 13:45

---